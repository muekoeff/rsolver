{
	/*
	class BinaryCondition {
		constructor( operator, leftOperand, rightOperand ) {
			this._type = 'BinaryCondition';
			this.operator = operator;
			this.leftOperand = leftOperand;
			this.rightOperand = rightOperand;
		}
	}

	class UnaryCondition {
		constructor( operator, operand ) {
			this._type = 'UnaryCondition';
			this.operator = operator;
			this.operand = operand;
		}
	}

	class Literal {
		constructor( literal ) {
			this._type = 'Literal';
			this.literal = literal;
		}
	}

	class RequestTuple {
		constructor( query, conditionList, propertyList ) {
			this._type = 'RequestTuple';
			this.query = query;
			this.conditionList = conditionList;
			this.propertyList = propertyList;
			}
	}

	var options = {
		BinaryCondition: BinaryCondition,
		UnaryCondition: UnaryCondition,
		Literal: Literal,
		RequestTuple: RequestTuple
	};
	*/
}

CommandList
	=	cmdHead:CommandLine
		cmdTail:(
			"\n"
			_cmdTail:( _cmdTail:CommandList {
				return _cmdTail
			} )?
			{
				return _cmdTail ?? null;
			}
		)?
		{
			const cmdList = cmdTail ?? [];
			cmdList.unshift( cmdHead );
			return cmdList;
		}

CommandLine
	=	(
			cmdHead:( CommandLineItem )
			cmdTail:(
				"\t"
				_cmdTail:CommandLine
				{
					return _cmdTail;
				}
			)?
			{
				const cmdList = cmdTail ?? [];
				cmdList.unshift( cmdHead );
				return cmdList;
			}
		) / (
			""
			{
				return [];
			}
		)

CommandLineItem = Literal / RequestTuple

Literal
	=	"$"
		literal:( [^$\t\n]+ )
		{
			return new options.Literal( literal.join( "" ) );
		}

RequestTuple
	=	searchQuery:SearchQuery
		condProp:(
			(
				"||"
			   	_properties:PropertyList
				{
					return [ null, _properties ];
				}
			) / (
				"|"
				_conditions:ConditionList
				properties:(
					"|"
					_properties:PropertyList
					{
						return _properties;
					}
				)?
				{
					return [ _conditions, properties ];
				}
			)
		)?
		{
			return new options.RequestTuple( searchQuery, condProp === null ? null : condProp[ 0 ], condProp === null ? null : condProp[ 1 ] );
		}

ConditionList
	= condHead:Condition
		condTail:(
			";"
			_condTail:ConditionList
			{
				return _condTail;
			}
		)?
		{
				const condList = condTail ?? [];
				condList.unshift( condHead );
				return condList;
		}

Condition
	= unaryCondition:UnaryCondition / binaryCondition:BinaryCondition

BinaryCondition
	= leftOperand:SearchQuery
		operator:BinaryConditionOperator
		rightOperand:SearchQuery
		{
			return new options.BinaryCondition( operator, leftOperand, rightOperand );
		}

BinaryConditionOperator = "=" / "*=" / "~"

UnaryCondition
	= operator:UnaryConditionOperator
		operand:SearchQuery
		{
			return new options.UnaryCondition( operator, operand );
		}

UnaryConditionOperator = "^"

PropertyList
	= propHead:PropertyAssignment
		propTail:(
			";"
			_propTail:PropertyList
			{
				return _propTail;
			}
		)?
		{
			if ( propTail === null ) {
			return propHead;
		} else {
			return { ...propHead, ...propTail };
		}
	}

PropertyAssignment
	= key:PropertyKey
		"="
		value:SearchQuery
		{
			const dict = {};
			dict[key] = value;
			return dict;
		}

PropertyKey
	= chars:[a-zA-Z0-9_]+
		{
			return chars.join( '' ).toLowerCase();
		}

SearchQuery
	= chars:([^\t\n\r\f\|\\=~^;] /
		(
			"\\\\"
			{
				return '\\';
			}
		) /
		(
			"\\="
			{
				return '=';
			}
	 	) / (
			"\\~"
			{
				return '~';
			}
		) / (
			"\\^"
			{
				return '^';
			}
		) / (
			"\\;"
			{
				return ';';
			}
		) / (
			"\\|"
			{
				return '|';
			}
		)
	)+
	{
		return chars.join( '' );
	}
