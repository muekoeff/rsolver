/* eslint-disable semi */
import Outputtable, { OutputMode } from './outputtable';

export default interface CommandItem extends Outputtable {
	getOutputStatus(): OutputMode;
}
