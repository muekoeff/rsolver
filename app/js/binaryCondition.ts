import { mount } from './app';
import Condition from './condition';
import * as Outputtable from './outputtable';
import Parser from './parser';
import RequestTuple from './requestTuple';
import { Status } from './requestTuple';
import { Response } from './searchEntities';
import TaskQueue from './taskQueue';

export default class BinaryCondition extends Condition {
	leftOperand: string;
	rightOperand: string;

	constructor( operator: string, leftOperand: string, rightOperand: string ) {
		super( operator );
		this.leftOperand = leftOperand;
		this.rightOperand = rightOperand;
	}

	generateCondition(): string {
		switch ( this.operator ) {
		case '=':
			return `?item wdt:${this.leftOperand} wd:${this.rightOperand} .`;
		case '*=':
			return `?item wdt:${this.leftOperand}* wd:${this.rightOperand} .`;
		case '~':
			return `?item ${this.leftOperand} ${this.rightOperand} .`;
		default:
			return null;
		}
	}

	getOutput( mode: Outputtable.OutputMode ): string {
		switch ( mode ) {
		case Outputtable.OutputMode.Failed:
			return `${Parser.encodeSearchQuery( this.leftOperand )}${this.operator}${Parser.encodeSearchQuery( this.rightOperand )}`;
		case Outputtable.OutputMode.Success:
		default:
			return '';
		}
	}

	async getRequest( taskQueue: TaskQueue, tuple: RequestTuple ): Promise<void> {
		try {
			const data: Response = await $.ajax( {
				data: {
					action: 'wbsearchentities',
					format: 'json',
					language: mount.settings.searchLanguage,
					limit: mount.settings.disambiguationCandidatesNumber,
					origin: '*',
					search: tuple.query,
					type: tuple.type,
					uselang: mount.settings.searchLanguage
				},
				url: 'https://www.wikidata.org/w/api.php'
			} );
			taskQueue.finishTask();

			if ( data.search.length === 0 ) {
				tuple.status = Status.NoResults;
			} else {
				const items = await Condition.filterItems( taskQueue, tuple, data.search, mount.settings.itemDescriptionLanguage );
				tuple.finaliseRequest( items );
			}
		} catch ( ex ) {
			taskQueue.finishTask();

			tuple.status = Status.Failed;
			throw ex;
		}
	}
}
