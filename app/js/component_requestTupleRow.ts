import { mount } from './app';
import CommandItem from './commandItem';
import DisambiguationItem from './disambiguationItem';
import Outputtable, { OutputMode } from './outputtable';
import RequestTuple from './requestTuple';
import { Status } from './requestTuple';
import TaskQueue from './taskQueue';

export default {
	computed: {
		note: function (): string {
			return this.tuple.getProperty( 'note' );
		},
		statusClass: function (): string {
			const tuple: RequestTuple = this.tuple;
			return `status-${tuple.status.replace( / /g, '-' )}`;
		},
		statusText: function (): string {
			const tuple: RequestTuple = this.tuple;
			return tuple.status;
		},
		tupleText: function (): string {
			const commandLine: CommandItem[] = this.commandline;
			return commandLine.map( ( command ) => {
				return command.getOutput( OutputMode.Failed );
			} ).join( '\t' );
		}
	},
	methods: {
		chooseDisambiguation: function ( tuple: RequestTuple, disambiguationItem: DisambiguationItem ): void {
			tuple.chooseDisambiguation( disambiguationItem );
		},
		clearResult: function ( tuple: RequestTuple ): void {
			tuple.chooseDisambiguation( null );
		},
		editQuery: function (): void {
			const tuple: RequestTuple = this.tuple;
			const newQuery = prompt( 'Please enter a new search query.', tuple.query );

			if ( ( newQuery ?? null ) !== null ) {
				tuple.editQuery( newQuery );
			}
		},
		removeCommandLine: function (): void {
			const commandLine: CommandItem[] = this.commandline;
			let isRemoved = null;
			for ( const command of commandLine ) {
				if ( command instanceof RequestTuple ) {
					if ( isRemoved === null ) {
						isRemoved = command.status === Status.Removed;
					}

					if ( isRemoved ) {
						command.status = Status.Pending;
					} else {
						command.disambiguationItems = [];
						command.result = null;
						command.status = Status.Removed;
					}
				}
			}
		},
		requestMore: function (): void {
			const taskQueue: TaskQueue = mount.taskQueue;
			const tuple: RequestTuple = this.tuple;

			tuple.requestMore( taskQueue );
		}
	},
	props: [ 'commandline', 'displaycommandline', 'lineindex', 'tuple', 'tupleindex' ],
	template: `<tbody>
		<tr>
			<th scope="row">{{lineindex + 1}}</th>
			<td v-bind:class="statusClass"><span class="status">{{statusText}}</span></td>
			<td>
				{{tuple.query}}
				<template v-if="note !== null">
					<br/><em>{{note}}</em>
				</template>
			</td>
			<td>
				<template v-if="tuple.result !== null">
					<a target="_blank" rel="noopener" v-bind:href="'https://www.wikidata.org/wiki/' + tuple.result" v-if="tuple.result !== null">{{tuple.result}}</a> <small>[<a href="#" title="Clear result" v-on:click.prevent="clearResult( tuple )">×</a>]</small>
				</template>
			</td>
			<td>
				<ul v-if="tuple.disambiguationItems.length > 0">
					<li v-for="disambiguationItem in tuple.disambiguationItems">
						<a target="_blank" rel="noopener" v-bind:class="{ chosen: tuple.result === disambiguationItem.entityId }" v-bind:href="'https://www.wikidata.org/wiki/' + disambiguationItem.entityId" v-on:click.prevent="chooseDisambiguation( tuple, disambiguationItem )">{{disambiguationItem.label}}</a> <small>{{disambiguationItem.description}}</small>
					</li>
				</ul>
			</td>
			<td>
				<ul class="list-inline">
					<li class="list-inline-item"><button type="button" class="btn btn-link btn-sm p-0" title="Edit search query" v-on:click.prevent="editQuery">🖋️</button></li>
					<li class="list-inline-item"><button type="button" class="btn btn-link btn-sm p-0" title="Remove this command line" v-on:click.prevent="removeCommandLine">🗑️</button></li>
					<li class="list-inline-item" v-if="tuple.working"><button type="button" class="btn btn-link btn-sm p-0" title="Loading…" style="cursor: progress;">⏳</button></li>
					<li class="list-inline-item" v-if="tuple.canLoadMore"><button type="button" class="btn btn-link btn-sm p-0" title="Load more search results" v-on:click.prevent="requestMore">➕</button></li>
				</ul>
			</td>
		</tr>
		<tr v-if="displaycommandline">
			<td></td>
			<td colspan="5">{{tupleText}}</td>
		</tr>
	</tbody>`
};
