export class Response {
	searchinfo: {
		search: string
	};
	search: Result[];
	success: number;
}

export class Result {
	id: string;
	title: string;
	pageid: number;
	display: {
		description: {
			language: string,
			value: string
		},
		label: {
			language: string,
			value: string
		}
	};
	repository: string;
	url: string;
	concepturi: string;
	label: string;
	description: string;
	match: {
		type: string,
		text: string
	};
	aliases?: string[];
}
