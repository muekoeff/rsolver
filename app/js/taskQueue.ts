export class StatusReporter {
	private _failTask: () => void;
	get failTask(): () => void {
		return this._failTask;
	}
	private _finishTask: () => void;
	get finishTask(): () => void {
		return this._finishTask;
	}

	constructor( finishTask: () => void, failTask: () => void ) {
		this._failTask = failTask;
		this._finishTask = finishTask;
	}
}

export default class TaskQueue {
	max: number;
	nextTaskPointer: number[];
	performing: number;
	queue: { [key: number]: ( ( _statusReporter: StatusReporter ) => void )[] };

	taskCounter: number;
	currentHighestPriority: number;
	prioritiesCache: string[];

	constructor( max: number ) {
		this.max = max;
		this.nextTaskPointer = [];
		this.queue = [];
		this.performing = 0;

		this.taskCounter = 0;
		this.currentHighestPriority = 0;
		this.prioritiesCache = [];
	}

	enqueueTask( priority: number ): Promise<StatusReporter> {
		return new Promise( ( resolve ) => {
			if ( typeof this.queue[ priority ] === 'undefined' ) {
				this.queue[ priority ] = [];
				this.prioritiesCache = Array.from( Object.keys( this.queue ) );
				this.prioritiesCache.sort( ( l: string, r: string ) => Number( r ) - Number( l ) );
				this.nextTaskPointer[ priority ] = 0;
			}
			this.queue[ priority ].push( resolve );
			if ( priority > this.currentHighestPriority ) {
				this.currentHighestPriority = priority;
			}
			this.taskCounter++;
			this.work();
		} );
	}

	finishTask(): void {
		this.performing -= 1;
		this.work();
	}

	setNextPriority(): void {
		const currentPriorityIndex = this.prioritiesCache.indexOf( String( this.currentHighestPriority ) );
		this.currentHighestPriority = Number( this.prioritiesCache[ currentPriorityIndex + 1 ] );
	}

	work(): void {
		if ( this.max > this.performing ) {
			if ( this.taskCounter <= 0 ) {
				this.nextTaskPointer = [];
				this.queue = [];
				this.taskCounter = 0;
				this.currentHighestPriority = 0;
				this.prioritiesCache = [];
			} else if ( this.nextTaskPointer[ this.currentHighestPriority ] >= this.queue[ this.currentHighestPriority ].length ) {
				// Priority is done
				this.setNextPriority();
			} else {
				const currentPriorityQueue = this.queue[ this.currentHighestPriority ];
				const inprogress = currentPriorityQueue[ this.nextTaskPointer[ this.currentHighestPriority ] ];

				inprogress( new StatusReporter( () => this.finishTask(), () => this.finishTask() ) );

				this.performing += 1;
				this.nextTaskPointer[ this.currentHighestPriority ]++;
				this.taskCounter--;
			}
		}
	}
}
