export default class QuerySamples {
	// eslint-disable-next-line no-undef
	private static samples: string[][] = require( '../data/querySamples.json' );

	static getRandomSample(): string {
		return QuerySamples.samples[ Math.floor( Math.random() * QuerySamples.samples.length ) ].join( '\n' );
	}
}
