import { mount } from './app';
import Condition from './condition';
import * as Outputtable from './outputtable';
import Parser from './parser';
import RequestTuple from './requestTuple';
import { Status } from './requestTuple';
import TaskQueue from './taskQueue';
import Util from './util';

export default class UnaryCondition extends Condition {
	operand: string;

	constructor( operator: string, operand: string ) {
		super( operator );
		this.operand = operand;
	}

	generateCondition(): string {
		return null;
	}

	getOutput( mode: Outputtable.OutputMode ): string {
		switch ( mode ) {
		case Outputtable.OutputMode.Failed:
			return `${this.operator}${Parser.encodeSearchQuery( this.operand )}`;
		case Outputtable.OutputMode.Success:
		default:
			return '';
		}
	}

	async getRequest( taskQueue: TaskQueue, tuple: RequestTuple ): Promise<void> {
		const host = Util.getWikiHost( this.operand );
		if ( host !== null ) {
			tuple.status = Status.Working;

			try {
				const data = await $.ajax( {
					data: {
						action: 'query',
						format: 'json',
						prop: 'pageprops',
						ppprop: 'wikibase_item',
						redirects: mount.settings.followRedirects ? '1' : '0',
						titles: tuple.query,
						origin: '*'
					},
					url: `https://${host}/w/api.php`
				} );
				taskQueue.finishTask();

				if ( Object.keys( data.query.pages )[ 0 ] === '-1' ) {
					tuple.status = Status.NoResults;
				} else {
					tuple.status = Status.Success;
					tuple.result = data.query.pages[ Object.keys( data.query.pages )[ 0 ] ].pageprops.wikibase_item;
				}
			} catch ( ex ) {
				taskQueue.finishTask();

				tuple.status = Status.Failed;
				throw ex;
			}
		} else {
			tuple.status = Status.Failed;
		}
	}
}
