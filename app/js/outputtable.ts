/* eslint-disable semi */
// eslint-disable-next-line no-shadow
export enum OutputMode {
	// eslint-disable-next-line no-unused-vars
	Failed,
	// eslint-disable-next-line no-unused-vars
	Success
}

export default interface Outputtable {
	// eslint-disable-next-line no-unused-vars
	getOutput( mode: OutputMode ): string;
}
