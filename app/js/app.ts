import { pipeOutput, setupApiListener } from './api';
// eslint-disable-next-line no-unused-vars
import bootstrap from 'bootstrap';
import CommandItem from './commandItem';
import Parser from './parser';
import QuerySamples from './querySamples';
import RequestTuple from './requestTuple';
import RequestTupleRow from './component_requestTupleRow';
import Outputtable, { OutputMode } from './outputtable';
import TaskQueue from './taskQueue';
import { createApp, reactive, watch } from 'vue';

// eslint-disable-next-line no-undef
require( 'tooltipster' );
// eslint-disable-next-line no-undef
require( 'tooltipster/dist/css/tooltipster.bundle.css' );
// eslint-disable-next-line no-undef
require( 'tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css' );

const app: any = createApp( {
	computed: {
		entityCompareUrl: function () {
			return `https://muekoeff.gitlab.io/entitycompare/?f&r&i=${encodeURIComponent( this.output.success.replace( /\n+/g, ',' ) )}`;
		},
		isEntityList: function () {
			if ( this.output.success === null ) {
				return false;
			} else {
				return /^Q[0-9]+(\nQ[0-9]+)*$/.test( this.output.success );
			}
		},
		statusClass: function ( tuple: RequestTuple ): string {
			return `status-${tuple.status.replace( / /g, '-' )}`;
		}
	},
	data: () => {
		return {
			commandLines: [],
			errors: {},
			example: null,
			filterStatus: [
				'disambiguation',
				'failed',
				'no results',
				'pending',
				'success'
			],
			input: '',
			languages: [],
			output: {
				failed: null,
				pipeEnabled: false,
				success: null
			},
			settings: {},
			taskQueue: null
		};
	},
	methods: {
		copy: function () {
			navigator.clipboard.writeText( this.output.success );
		},
		copyFailed: function () {
			navigator.clipboard.writeText( this.output.failed );
		},
		copyQuickStatements: function () {
			navigator.clipboard.writeText( this.output.success );
			window.open( 'https://quickstatements.toolforge.org/' );
		},
		displayGrammar: function () {
			const grammarModal = document.getElementById( 'modal-grammar' );

			if ( grammarModal.dataset.ready !== 'true' ) {
				grammarModal.dataset.ready = 'true';

				// eslint-disable-next-line no-undef
				const grammkit = require( 'grammkit' );
				// eslint-disable-next-line no-undef
				const parse = require( 'peggy/lib/parser' ).parse;
				const grammar = parse( Parser.grammar );

				const grammarBody = grammarModal.querySelector( '.modal-body' );
				for ( const rule of grammar.rules ) {
					grammarBody.insertAdjacentHTML( 'beforeend', `<div><h3>${rule.name}</h3><div style="overflow-x:auto">${grammkit.diagram( rule )}</div></div>` );
				}
			}
		},
		exampleSetup: function (): void {
			this.example = QuerySamples.getRandomSample();
		},
		exampleUse: function (): void {
			// eslint-disable-next-line no-jquery/no-global-selector
			const $button = $( '#button-useexample' );

			// eslint-disable-next-line no-jquery/no-class-state
			if ( this.input === '' || $button.hasClass( 'btn-danger' ) ) {
				this.input = this.example;
				// eslint-disable-next-line no-jquery/no-class-state
				if ( $button.hasClass( 'tooltipstered' ) ) {
					$button.tooltipster( 'destroy' );
				}
				$button.tooltipster( {
					content: 'Ready! Now press “Resolve”.',
					functionAfter: function () {
						$button.addClass( 'btn-secondary' ).removeClass( 'btn-success' );
					},
					functionBefore: function () {
						$button.addClass( 'btn-success' ).removeClass( 'btn-danger btn-secondary' );
					},
					theme: [ 'tooltipster-light', 'tooltipster-success' ],
					timer: 10000,
					trigger: 'custom'
				} ).tooltipster( 'open' );
			} else {
				const textSpan = document.createElement( 'span' );
				textSpan.innerHTML = 'Please clear your entered commands first.<br/><small>Just to make sure you didn\'t accidentially press this button.';

				// eslint-disable-next-line no-jquery/no-class-state
				if ( $button.hasClass( 'tooltipstered' ) ) {
					$button.tooltipster( 'destroy' );
				}
				$button.tooltipster( {
					content: textSpan,
					functionAfter: function () {
						$button.addClass( 'btn-secondary' ).removeClass( 'btn-danger' );
					},
					functionBefore: function () {
						$button.addClass( 'btn-danger' ).removeClass( 'btn-secondary btn-success' );
					},
					theme: [ 'tooltipster-light', 'tooltipster-error' ],
					timer: 10000,
					trigger: 'custom'
				} ).tooltipster( 'open' );
			}
		},
		failedToInput: function () {
			this.input = this.output.failed;
		},
		filterTuple: function ( commandLine: CommandItem[] ) {
			return commandLine.filter( ( command ) => {
				return command instanceof RequestTuple;
			} );
		},
		generateOutput: function () {
			const commandLines: CommandItem[][] = this.commandLines;
			const filteredCommandLines = commandLines.filter( ( commandLine ) => commandLine !== null );

			const outCommandLines: string[] = [];
			const outFailedCommandLines: string[] = [];

			for ( const commandLine of filteredCommandLines ) {
				const containsRemoved = commandLine.some( ( outputtable ) => {
					return outputtable instanceof RequestTuple && outputtable.status === 'removed';
				} );
				if ( containsRemoved ) {
					// Skip, if contains removed tuple
					continue;
				}

				const result = commandLine.reduce( ( accumulator, tuple ) => {
					return accumulator === OutputMode.Success ? tuple.getOutputStatus() : accumulator;
				}, OutputMode.Success );

				const lineOutput = commandLine.map( ( outputtable ) => {
					if ( outputtable.getOutputStatus() === OutputMode.Success && result === OutputMode.Failed ) {
						return `$${Parser.encodeSearchQuery( outputtable.getOutput( outputtable.getOutputStatus() ) )}`;
					} else {
						return outputtable.getOutput( outputtable.getOutputStatus() );
					}
				} ).join( '\t' );

				if ( result === OutputMode.Success ) {
					outCommandLines.push( lineOutput );
				} else {
					outFailedCommandLines.push( lineOutput );
				}
			}

			this.output.success = outCommandLines.join( '\n' );
			this.output.failed = outFailedCommandLines.join( '\n' );

			const output = outCommandLines.join( '\n' );

			// Only if list of Wikidata IDs
			if ( /^Q[0-9]+(\nQ[0-9]+)*$/.test( output ) ) {
				this.output.entityList = encodeURIComponent( outCommandLines.join( ',' ) );
			} else {
				this.output.entityList = null;
			}

			if ( this.output.pipeEnabled ) {
				pipeOutput( this.output.success, this.output.failed );
			}
		},
		parseAndResolve: function () {
			this.parseInput();
			this.resolveTuples();
		},
		parseInput: function () {
			const parser = new Parser();
			try {
				this.commandLines = parser.parse( this.input ).map( ( commandLine: CommandItem[] ) => {
					return commandLine.map( ( command: Outputtable ) => {
						return reactive( command );
					} );
				} );
				delete this.errors[ 'parser-error' ];
			} catch ( ex ) {
				console.error( ex );
				let location = null;
				if ( ex.location !== undefined ) {
					location = location = `Line ${ex.location.start.line},${ex.location.start.column}`;

					if ( ex.location.start.line !== ex.location.end.line || ex.location.start.column !== ex.location.end.column ) {
						location += ` to ${ex.location.end.line},${ex.location.end.column}`;
					}
				}
				this.errors[ 'parser-error' ] = [ ex.name, `${ex.message}${location !== null ? ` (${location})` : ''}` ];
			}
		},
		resolvePendingTuples: async function () {
			this._setupTaskQueue();

			const commandLines: CommandItem[][] = this.commandLines;
			const tuples = <RequestTuple[]> commandLines.map( ( commandLine ) => {
				return commandLine.filter( ( tuple ) => {
					return tuple instanceof RequestTuple && tuple.status === 'pending';
				} );
			} ).flat();

			await Promise.allSettled( tuples.map( ( tuple ) => tuple.request( this.taskQueue ) ) );
		},
		resolveTuples: async function () {
			this._setupTaskQueue();

			const commandLines: CommandItem[][] = this.commandLines;
			const tuples = <RequestTuple[]> commandLines.map( ( commandLine ) => {
				return commandLine.filter( ( tuple ) => {
					return tuple instanceof RequestTuple;
				} );
			} ).flat();

			await Promise.allSettled( tuples.map( ( tuple ) => tuple.request( this.taskQueue ) ) );
		},
		settingsDefault: function () {
			return {
				disambiguationCandidatesNumber: 7,
				displayCommandLine: false,
				filterProgressbar: true,
				followRedirects: true,
				groupProgressbarStatus: true,
				itemDescriptionLanguage: 'en',
				maximumConcurrentRequests: 4,
				renameKeepAsNote: false,
				searchLanguage: 'en',
				splitParseResolve: false
			};
		},
		settingsFromLocalStorage: function (): any {
			const settingsRaw = localStorage.getItem( 'settings' );
			if ( settingsRaw === null ) {
				return {};
			} else {
				return JSON.parse( settingsRaw );
			}
		},
		settingsFromQuery: function (): any {
			const searchParams = ( new URL( window.location.href ) ).searchParams;
			const query = searchParams.get( 'settings' ) ?? searchParams.get( 's' );
			if ( query === null ) {
				return {};
			} else {
				try {
					return JSON.parse( query );
				} catch ( ex ) {
					console.error( ex );
					return {};
				}
			}
		},
		settingsLoad: function () {
			// Load settings
			this.settings = reactive( { ...this.settingsDefault(), ...this.settingsFromLocalStorage(), ...this.settingsFromQuery() } );
			this.settingsSetupLanguages();

			// Setup watcher
			watch( this.settings, () => {
				localStorage.setItem( 'settings', JSON.stringify( this.settings ) );
			} );
		},
		settingsRedirect: function (): void {
			// eslint-disable-next-line no-jquery/no-global-selector
			const $button = $( '#button-settings-geturl' );

			// eslint-disable-next-line no-jquery/no-class-state
			if ( $button.hasClass( 'btn-danger' ) ) {
				window.location.search = `?s=${encodeURIComponent( JSON.stringify( this.settings ) )}`;
			} else {
				$button.tooltipster( {
					content: 'Clicking again on this button will redirect you to a new bookmarkable url which contains your settings. However, your current input will be lost once redirected.',
					functionAfter: () => {
						$button.addClass( 'btn-secondary' ).removeClass( 'btn-danger' );
					},
					functionBefore: () => {
						$button.addClass( 'btn-danger' ).removeClass( 'btn-secondary' );
					},
					side: 'left',
					theme: [ 'tooltipster-light', 'tooltipster-error' ],
					timer: 10000,
					trigger: 'custom'
				} ).tooltipster( 'open' );
			}
		},
		settingsSetupLanguages: function (): void {
			// https://www.wikidata.org/w/api.php?action=help&modules=wbsearchentities
			// eslint-disable-next-line no-undef
			const languages: string[] = require( '../data/wikiLanguageCodes.json' );

			this.languages = languages;
		},
		_setupTaskQueue: function (): void {
			if ( this.taskQueue === null ) {
				this.taskQueue = new TaskQueue( this.settings.maximumConcurrentRequests );
			}
		}
	},
	mounted: function (): void {
		this.settingsLoad();
		this.exampleSetup();

		setupApiListener();
	},
	updated: function (): void {
		// eslint-disable-next-line no-jquery/no-global-selector
		const $picker = <any>$( '.v-selectpicker' );
		$picker.selectpicker( 'refresh' );
	}
} );
app.component( 'tuple-row', RequestTupleRow );
export const mount = app.mount( '#content' );

// @source https://sumtips.com/snippets/javascript/tab-in-textarea/#jq
document.getElementById( 'commands' ).addEventListener( 'keydown', function ( e ) {
	if ( e.key === 'Tab' ) {
		e.preventDefault();
		const input = <HTMLInputElement> this;

		// get caret position/selection
		const start = input.selectionStart;
		const end = input.selectionEnd;

		input.value = `${( input.value ).slice( 0, Math.max( 0, start ) )}\t${input.value.slice( Math.max( 0, end ) )}`;

		return false;
	}
} );
