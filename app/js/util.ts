export default class Util {
	static PRIORITY_MEDIAWIKI_API = 3;
	static PRIORITY_WIKIDATA_API_CONDITION = 3;
	static PRIORITY_WIKIDATA_API_REQUESTTUPLE = 1;
	static PRIORITY_WIKIDATA_QUERY_SERVICE = 3;

	static getWikiHost( name: string ): string {
		if ( name === 'commonswiki' ) {
			return 'commons.wikimedia.org';
		}
		if ( name === 'mediawikiwiki' ) {
			return 'www.mediawiki.org';
		}
		if ( name === 'metawiki' ) {
			return 'meta.wikimedia.org';
		}
		if ( name === 'specieswiki' ) {
			return 'species.wikimedia.org';
		}
		if ( name === 'wikidatawiki' ) {
			return 'wikidata.org';
		}
		if ( name === 'wikimaniawiki' ) {
			return 'wikimania.wikimedia.org';
		}

		let match = name.match( /^([a-zA-Z_]+)(wikibooks|wikinews|wikiquote|wikisource|wikiversity|wikivoyage|wiktionary)$/ );
		if ( match !== null ) {
			return `${match[ 1 ]}.${match[ 2 ]}.org`;
		}

		match = name.match( /^([a-zA-Z_]+)wiki$/ );
		if ( match !== null ) {
			return `${match[ 1 ]}.wikipedia.org`;
		}

		console.error( 'Unknown wiki: ', name );
		return null;
	}
}
