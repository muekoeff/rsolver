const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );

module.exports = {
	devtool: 'source-map',
	entry: './app/js/app.ts',
	externals: {
		jquery: 'jQuery'
	},
	mode: 'development',
	module: {
		rules: [
			{ test: /\.css$/i, use: [ 'style-loader', 'css-loader' ] },
			{
				test: /\.ejs$/,
				loader: 'ejs-loader',
				options: {
					esModule: false
				}
			},
			{ test: /\.pegjs$/i, use: 'raw-loader' },
			{ test: /\.svg$/i, use: [
				{
					loader: 'file-loader'
				}
			] },
			{ test: /\.tsx?$/i, use: 'ts-loader' },
			{ test: /\.txt$/i, use: 'raw-loader' }
		]
	},
	output: {
		path: __dirname + '/public',
		filename: '[name].js'
	},
	plugins: [
		new CleanWebpackPlugin( {
			cleanAfterEveryBuildPatterns: [ 'public' ]
		} ),
		new CopyWebpackPlugin( {
			patterns: [
				{ from: './app/css/**/*', to: 'css/[name][ext]' },
				{ from: './node_modules/@popperjs/core/dist/umd/popper.min.js', to: 'js/popper.min.js' },
				{ from: './node_modules/@popperjs/core/dist/umd/popper.min.js.map', to: 'js/popper.min.js.map' },
				{ from: './node_modules/bootstrap/dist/css/bootstrap.min.css', to: 'css/bootstrap.min.css' },
				{ from: './node_modules/bootstrap/dist/css/bootstrap.min.css.map', to: 'css/bootstrap.min.css.map' },
				{ from: './node_modules/bootstrap/dist/js/bootstrap.min.js', to: 'js/bootstrap.min.js' },
				{ from: './node_modules/bootstrap/dist/js/bootstrap.min.js.map', to: 'js/bootstrap.min.js.map' },
				{ from: './node_modules/bootstrap-select/dist/css/bootstrap-select.min.css', to: 'css/bootstrap-select.min.css' },
				{ from: './node_modules/bootstrap-select/dist/js/bootstrap-select.min.js', to: 'js/bootstrap-select.min.js' },
				{ from: './node_modules/bootstrap-select/dist/js/bootstrap-select.min.js.map', to: 'js/bootstrap-select.min.js.map' },
				{ from: './node_modules/jquery/dist/jquery.min.js', to: 'js/jquery.min.js' },
				{ from: './node_modules/jquery/dist/jquery.min.map', to: 'js/jquery.min.map' },
				{ from: './node_modules/vue/dist/vue.global.js', to: 'js/vue.global.js' },
				{ from: './node_modules/vue/dist/vue.global.prod.js', to: 'js/vue.global.prod.js' },
				{ from: './toolinfo.json', to: 'toolinfo.json' }
			]
		} ),
		new HtmlWebpackPlugin( {
			template: './app/index.html',
			title: 'Rsolver for Wikidata'
		} )
	],
	resolve: {
		extensions: [ '.css', '.js', '.svg', '.ts', '.tsx' ]
	}
};
